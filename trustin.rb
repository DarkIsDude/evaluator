require "json"
require "net/http"

class TrustIn
  def initialize(evaluations)
    @evaluations = evaluations
  end

  def update_durability()
    @evaluations.each do |evaluation|
      if ["SIREN", "VAT"].index(evaluation.type) == nil
        return
      end

      if evaluation.readyForANewEvaluation? || evaluation.updateAsked?
        newEvaluation(evaluation)
        return
      end

      if evaluation.durability <= 0
        return
      end

      if evaluation.isFavorable?
        evaluation.changeDurability(-1)
      elsif evaluation.reachAPIError?
        if (evaluation.durability >= 50)
          evaluation.changeDurability(evaluation.isSIREN() ? -5 : -1)
        else
          evaluation.changeDurability(evaluation.isSIREN() ? -1 : -3)
        end
      end
    end
  end

  def newEvaluation(evaluation)
    uri = URI("https://public.opendatasoft.com/api/records/1.0/search/?dataset=sirene_v3" \
      "&q=#{evaluation.value}&sort=datederniertraitementetablissement" \
      "&refine.etablissementsiege=oui")
    response = Net::HTTP.get(uri)
    parsed_response = JSON.parse(response)
    company_state = parsed_response["records"].first["fields"]["etatadministratifetablissement"]
    if company_state == "Actif"
      evaluation.state = "favorable"
      evaluation.reason = "company_opened"
      evaluation.durability = 100
    else
      evaluation.state = "unfavorable"
      evaluation.reason = "company_closed"
      evaluation.durability = 100
    end
  end
end

class Evaluation
  attr_accessor :type, :value, :durability, :state, :reason

  def initialize(type:, value:, durability:, state:, reason:)
    @type = type
    @value = value
    @durability = durability
    @state = state
    @reason = reason
  end

  def changeDurability(by)
    # TODO Are we sure HERE ?
    if @durability + by > 0
      @durability = @durability + by
    else
      @durability = 0
    end
  end

  def readyForANewEvaluation?
    # TODO Should I change with only == ?
    return @durability <= 0 && @state == "favorable"
  end

  def updateAsked?
    return @durability > 0 && @state == "unconfirmed" && @reason == "ongoing_database_update"
  end

  def reachAPIError?
    return @state == "unconfirmed" && @reason == "unable_to_reach_api"
  end

  def isFavorable?
    return @state == "favorable"
  end

  def isSIREN
    return @type == "SIREN"
  end

  def to_s()
    "#{@type}, #{@value}, #{@durability}, #{@state}, #{@reason}"
  end
end
